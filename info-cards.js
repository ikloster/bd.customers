(function() {
    window.M2_CUSTOMERS_CARDS_INFO = [
        {
            title: 'Способ оплаты',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/payment.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/shopping-at-ikea/oplachivaite-pokupki-legko-i-udobno-pubb1535691'
        },
        {
            title: 'Подарочные карты',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/giftcard.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/services/finance-options/podarochnaya-karta-pub64a734f1'
        },
        {
            title: 'Гарантия',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/exchange.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/garantiya-pub42a22eb1'
        },
        {
            title: 'Символы и обозначения',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/check_avalibility.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/simvoly-i-oboznacheniya-pubb47f74e1'
        },
        {
            title: 'Переработка вещей',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/recycle.png',
            link: 'https://m2.ikea.com/ru/ru/customer-service/services/removal-recycling/'
        },
        {
            title: 'Как делать покупки',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/purchases.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/kak-delat-pokupki-v-ikea-pube8c681d1'
        },
        {
            title: 'Отслеживание заказа',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/order_tracking.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/services/delivery/otslezhivanie-zakazov-pub02f35ca1'
        },
        {
            title: 'Заявка на ремонт',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/repairs.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/returns-claims/zayavka-na-remont-bytovoi-tekhniki-pubb6362fb1'
        },
    ]
})()