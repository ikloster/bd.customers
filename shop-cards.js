(function() {
    window.M2_CUSTOMERS_CARDS_SHOP = [
        {
            title: 'Как связаться с ИКЕА',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/interior_design_advice.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/contact-us/'
        },
        {
            title: 'Удобства для каждого',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/purchases.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/shopping-at-ikea/udobstva-dlya-kazhdogo-pub30d33f61'
        },
        {
            title: 'Все для легкого шопинга',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/shopping.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/shopping-at-ikea/vse-dlya-legkogo-shopinga-pub5bed75a1'
        },
        {
            title: 'В ИКЕА всегда рады детям!',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/toy.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/shopping-at-ikea/smaland/'
        },
        {
            title: 'Забери домой вкус Швеции',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/swedish_flavor.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/shopping-at-ikea/zaberi-domoi-vkus-shvecii-pub01bca281'
        },
        {
            title: 'Шведская кухня в ресторане ИКЕА',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/swedish_kitchen.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/shopping-at-ikea/nasladites-tradicionnoi-shvedskoi-kukhnei-v-restorane-ikea-pubf6ba78e1'
        },
        {
            title: 'Каталог и брошюры',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/catalog.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/catalogues/'
        },
    ]
})()