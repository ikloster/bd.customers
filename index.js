Vue.component("card", {
  template: "#card-template",
  props: {
    card: Object
  }
});

var app = new Vue({
  el: "#app",
  data: {
    serviceCards: window.M2_CUSTOMERS_CARDS_SERVICE,
    infoCards: window.M2_CUSTOMERS_CARDS_INFO,
    shopCards: window.M2_CUSTOMERS_CARDS_SHOP,
  }
});
