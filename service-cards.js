(function() {
    window.M2_CUSTOMERS_CARDS_SERVICE = [
        {
            title: 'Доставка и самовывоз',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/delivery.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/dostavka-iz-internet-magazina-pub30f2c801'
        },
        {
            title: 'Обмен и возврат',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/exchange.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/returns-claims/'
        },
        {
            title: 'Замер',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/measuring.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/services/zamer-pomesheniya-pubc059c302'
        },
        {
            title: 'Сборка и установка',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/assembly.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/services/assembly/'
        },
        {
            title: 'Сервис YouDo',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/assembly.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/services/servis-youdo-pub8d515f11'
        },
        {
            title: 'Планирование',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/planning.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/planirovanie-pubb82b8cd1'
        },
        {
            title: 'Дизайн-сервис ИКЕА',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/interior_design_advice.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/consult-pub1b4bb1a1'
        },
        {
            title: 'Пошив изделий',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/sewing.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/services/poshiv-izdelii-pubf6770921'
        },
        {
            title: 'Рассрочка и кредит',
            image: 'https://bd.ikea.ru/ext/ru/customers/images/payment.svg',
            link: 'https://m2.ikea.com/ru/ru/customer-service/services/finance-options/rassrochka-pube9cc84b1'
        },
    ]
})()